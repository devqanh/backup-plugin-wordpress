<?php
/*
Plugin Name: custom shortcode
Plugin URI: http://devqanh/
Description: shortcode custom
Version: 1.0
Author: DEV
*/
 $dir = plugin_dir_url( __FILE__ )  ;
function show_ve($args)
{
 	$args    = array(
        'post_type' => $args['post_type'],
        'posts_per_page' => $args['numberpost']
    );
    query_posts($args);
	$content = "<div class='row'>";
    if (have_posts()): 
        while (have_posts()):   the_post();
            $content .= "<div class='col medium-4 small-12 large-4>'"; 
            $content .= '<div class="col-inner">';
            $content .= '<div class="item" style="border:1px solid #d8d7d7; background:white;">
   <img src="'.get_the_post_thumbnail_url().'" style="width:100%; "></p>
   <div class="price" style="text-align:center; padding:10px 15px; font-size:24px;"><span style="color:#d60000;">'.sympol_price(get_field('gia_ve')).' VNĐ</span></div>
   <div class="line" style="margin: 0 auto;height:1px; background:#d8d7d7; margin: 0px 25px; "></div>
   <div class="info" style="padding:15px 25px;">
      <div class="row">
         <div class="col small-5"><img src="'.get_field('logo_hang_bay').'" style="padding: 13px 0px;"></div>
         <div class="col small-7">
            <div class="line_1" style="width:100%; margin-bottom:10px;float:left;font-size: 18px;"><img src="'.$GLOBALS['dir'].'/images/flight_dep.png" style="float:left; margin-right:20px;"><span style="float:left;">'.get_field('dia_diem_bay').'</span><span style="float:right;">'.date('d/m',strtotime(get_field('ngay_di'))).'</span></p></div>
            <div class="line_1" style="width:100%; margin-bottom:10px;float:left;font-size: 18px;"><img src="'.$GLOBALS['dir'].'/images/flight_ret.png" style="float:left; margin-right:20px;"><span style="float:left;">'.get_field('dia_diem_den').'</span><span style="float:right;">'.date('d/m',strtotime(get_field('ngay_ve_2'))).'</span></div>
            </p>
         </div>
         </p>
      </div>
      </p>
   </div>
   <div class="dat_ve" style="text-align:center; margin-bottom:15px;"><a class="btn btn-search" style="background:#2aaae1; color:white;padding: 6px 40px; border-radius:0px;">ĐẶT VÉ</a></div>
   </p>
</div>
</div>';
		endwhile;
		wp_reset_query();
        return $content;
    else:
        $content .= "Không có bài viết nào được đăng";
        return $content;
    endif;
}
add_shortcode('ve_du_lich', 'show_ve');
function sympol_price($priceFloat) {
$symbol_thousand = '.';
$decimal_place = 0;
$price = number_format($priceFloat, $decimal_place, '', $symbol_thousand);
return $price;
}