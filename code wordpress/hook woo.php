
1
down vote
accepted
Let's look on the Visual Hook Guide. The price range is a part of the single product summary. The woocommerce_single_product_summary hook consists of:

 * @hooked woocommerce_template_single_title - 5
 * @hooked woocommerce_template_single_rating - 10
 * @hooked woocommerce_template_single_price - 10
 * @hooked woocommerce_template_single_excerpt - 20
 * @hooked woocommerce_template_single_add_to_cart - 30
 * @hooked woocommerce_template_single_meta - 40
 * @hooked woocommerce_template_single_sharing - 50
And the woocommerce_template_single_price function adds the single-product/price.php template-part.

So we have two ways:

1. Remove this template part for variable products only

add_action( 'woocommerce_before_single_product', 'my_remove_variation_price' );
function my_remove_variation_price() {
  global $product;
  if ( $product->is_type( 'variable' ) ) {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price' );
  }
}
2. Or edit the template part itself

Copy wp-content/plugins/woocommerce/templates/single-product/price.php to wp-content/themes/YOUR_THEME/woocommerce/single-product/price.php and replace this code fragment

  <p class="price"><?php echo $product->get_price_html(); ?></p>
with this one

<?php if ( ! $product->is_type( 'variable' ) ) : ?>
  <p class="price"><?php echo $product->get_price_html(); ?></p>
<?php endif; ?>