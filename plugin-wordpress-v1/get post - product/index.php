<?php
// Add custom Theme Functions here
function product_customer($arg) {
	$content = '';
	$args = array(
		//'order' => 'ASC',
		'post_type' => 'product',
		'product_cat' => $arg['id'],
		'posts_per_page' => $arg['amout']
	);
	query_posts($args);
	if ( have_posts() ) : 
		$i=0;
		$content.='<div class="product_top"><div class="container"><div class="row">';
		while ( have_posts() ) : $i++;
			the_post();
			$content .= '<div class="item"><div class="box-product">'; 
			
			if ( '' !== get_the_post_thumbnail() ) : 
				$content .='<div class="post-thumbnail">
					<a href="'. esc_url( get_permalink() ) .'">
						'.get_the_post_thumbnail().'
					</a>
				</div>';
			endif;
			
			$content .='<div class="text-hover">
				<span class="shadow"></span>
				<div class="box-text">
					<div class="box-text-inner">
						<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">'.get_the_title().'</a></h2><div class="desc">'.get_the_excerpt().'</div><p class="link-more"><a href="'.get_permalink().'"> Read more </a></p></div>
				</div>
			</div>';	
	
			$content .= '</div></div>';
		endwhile;
		$content .= '</div></div></div>';
		wp_reset_query();
	endif;
	return $content;
}
add_shortcode( 'product_top', 'product_customer' );
function product_special($arg) {
	$content = '';
	$args = array(
		//'order' => 'ASC',
		'post_type' => 'product',
		'product_cat' => $arg['id'],
		'posts_per_page' => $arg['amout']
	);
	query_posts($args);
	if ( have_posts() ) : 
		$i=0;
		$content.='<div class="product_special"><div class="container">';
		while ( have_posts() ) : $i++;
			the_post();
			$content .= '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '">'.get_the_title().'</a></h2>';
			$content .= '<div class="row"><div class="item_special"><div class="box-product">'; 
			
			if ( '' !== get_the_post_thumbnail() ) : 
				$content .='<div class="post-thumbnail">
					<a href="'. esc_url( get_permalink() ) .'">
						'.get_the_post_thumbnail().'
					</a>
				</div>';
			endif;
			$content .= '</div></div>';
			$content .= '<div class="item_special"><div class="box-product">'; 
			$content .='<div class="desc">'.get_the_excerpt().'</div>
			<p class="link-more"><a href="'.get_permalink().'"> Read more </a></p>
				</div></div>';	
	
			$content .= '</div></div></div>';
		endwhile;
		$content .= '</div></div>';
		wp_reset_query();
	endif;
	return $content;
}
add_shortcode( 'product_specials', 'product_special' );

?>

<?php query_posts('cat=17'); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
   <?php the_content(); ?>
<?php endwhile; endif; ?>