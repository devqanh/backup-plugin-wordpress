<?php
function register_count_text_setting()
{
    register_setting('count-text-group', 'count_text_time');
    register_setting('count-text-group', 'count_text_time2');
}

function count_text_create_menu()
{
    add_menu_page('Count text', 'Count text', 'administrator', __FILE__, 'count_text_page_setting', 'dashicons-editor-spellcheck', 85);
    add_action('admin_init', 'register_count_text_setting');

}

add_action('admin_menu', 'count_text_create_menu');
function count_text_page_setting()
{
    ?>
    <div class="wrap">
        <h2>Option count text post</h2>
        <?php if (isset($_GET['settings-updated'])) { ?>
            <div id="message" class="updated">
                <p><strong><?php _e('Save Changed') ?></strong></p>
            </div>
        <?php } ?>
        <form method="post" action="options.php">
            <?php settings_fields('count-text-group'); ?>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row">Count text</th>
                    <td><input min =1 size="20" type="number" name="count_text_time" placeholder=""
                               value="<?php echo get_option('count_text_time'); ?>"/>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">Count text</th>
                    <td><input min = 1 size="20" type="number" name="count_text_time2" placeholder=""
                               value="<?php echo get_option('count_text_time2'); ?>"/>
                    </td>
                </tr>
            </table>
            <?php submit_button(); ?>
        </form>
    </div>
<?php } ?>
