<?php
/**
 * Plugin Name: Get video from youtube
 * Plugin URI: https://devqanh.com
 * Description: shortcode get video from youtube ,[jw_player id="" height="300" ]
 * Author: Devqanh
 * Version: 1.0
 * Author URI: https://devqanh.com
 */
@set_time_limit(0);
function script_jw_player()
{

    wp_register_script('custom-script', plugins_url('/asset/js/jwplayer.js', __FILE__));
    wp_register_script('custom-script2', plugins_url('/asset/js/jw7-hlsjs-provider.js', __FILE__));
    wp_enqueue_script('custom-script');
    wp_enqueue_script('custom-script2');
   // wp_enqueue_style('style', plugins_url('/asset/css/style.css', __FILE__));
}

add_action('wp_enqueue_scripts', 'script_jw_player', 33);

function get_youtube ($id,$quality) {
parse_str(file_get_contents('http://www.youtube.com/get_video_info?video_id='.$id), $info);
$data = explode(',', $info['url_encoded_fmt_stream_map']);
foreach ($data as $value) :

	parse_str($value,$type) ;
	
	if($type['quality']==$quality) :
		return $type['url']; 
	endif; 
endforeach; 	

}
//echo get_youtube('zXpCUTPmAp8', 'medium'); 

function create_shortcode_jw($args, $content)
{
	 if(isset($args['id'])) :

		if(get_youtube($args['id'],'hd720')) :
			$link720  = get_youtube($args['id'],'hd720'); 
		endif;
		if (get_youtube($args['id'],'medium')):
			$link480 = get_youtube($args['id'],'medium'); 
		endif;
		if (get_youtube($args['id'],'small')) : 
			$link360 = get_youtube($args['id'],'small');
		endif;

	
	endif; 
        $html = "  <div id='my'></div>
        <script type='text/javascript'>jwplayer.key = 'dWwDdbLI0ul1clbtlw+4/UHPxlYmLoE9Ii9QEw=='</script> " ; 
        $html .= "    <script type='text/javascript'>
	jwplayer.defaults = {
    aspectratio: '16:9',
                autostart: false,
                controls: true,
                displaydescription: false,
                displaytitle: false,

                height: {$args['height']},
                mute: false,
                primary: 'html5',
                repeat: false,
                skin: 
                { 'name':'bekle'},
                stagevideo: false,
                stretching: 'uniform',
                width: '100%',
                
            }; 
             var playerInstance = jwplayer('my');

            playerInstance.setup({ 
            sources: [ 
                    {
                        file: '{$link720}',
                        label: '720p',
                        provider: 'http',
                        type: 'mp4'   
                        
                    },
                    {
                        file: '{$link480}',
                        label: '480p',
                        provider: 'http',
                        type: 'mp4'   
                        
                    },
                    {
                        file: '{$link360}',
                        label: '360p',
                        provider: 'http',
                        type: 'mp4'   
                        
                    }
                ] });


            </script> ";
return $html; 
}


add_shortcode('jw_player', 'create_shortcode_jw');
