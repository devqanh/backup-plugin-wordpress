<?php 

/*

Plugin Name: Quản lý hệ thống

Plugin URI: http://devqanh.com

Description: Hệ thống Quản trị

Author: Võ quyền anh

Version: 1.0

Author URI: http://devqanh.com
*/

/** Remove title Wordpress**/

add_filter('admin_title', 'my_admin_title', 10, 2);

add_action('admin_head','custom_css');

function custom_css(){
    echo '<style>#wpadminbar {background: #33495d!important;}</style>'; 
}

function my_admin_title($admin_title, $title)

{

    return $title.' &lsaquo; '.get_bloginfo('name').' &lsaquo; '.'Võ quyền anh . Theo mã nguồn wordpress';

}

/** Footer **/

function remove_footer_admin () {

    echo 'Phát triển theo mã nguồn wordpress . Liên hệ : <a href="https://www.facebook.com/devqanh96"> Quyền anh </a>';

}

add_filter('admin_footer_text', 'remove_footer_admin');

/** **/ 

//add_action('admin_init', 'rw_remove_dashboard_widgets');

    function rw_remove_dashboard_widgets() {

        remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); // right now

        remove_meta_box('dashboard_activity', 'dashboard', 'normal');// WP 3.8

        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // recent comments

        remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); // incoming links

        remove_meta_box('dashboard_plugins', 'dashboard', 'normal'); // plugins

         

        remove_meta_box('dashboard_quick_press', 'dashboard', 'normal'); // quick press

        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'normal'); // recent drafts

        remove_meta_box('dashboard_primary', 'dashboard', 'normal'); // wordpress blog

        remove_meta_box('dashboard_secondary', 'dashboard', 'normal'); // other wordpress news

}

/** Remove  WordPress Welcome Panel **/

remove_action('welcome_panel', 'wp_welcome_panel');



/** Ẩn Menu **/

add_action( 'admin_menu', 'my_remove_menu_pages' );

	function my_remove_menu_pages() {

		remove_menu_page('edit-comments.php');

        remove_menu_page('tools.php');

        remove_menu_page('');   	

	}

    

function remove_admin_bar_links() {

    global $wp_admin_bar;

    $wp_admin_bar->remove_menu('wp-logo');          /** Remove the WordPress logo **/

    $wp_admin_bar->remove_menu('about');            /** Remove the about WordPress link **/

    $wp_admin_bar->remove_menu('wporg');            /** Remove the WordPress.org link **/

    $wp_admin_bar->remove_menu('documentation');    /** Remove the WordPress documentation link **/

    $wp_admin_bar->remove_menu('support-forums');   /** Remove the support forums link **/

    $wp_admin_bar->remove_menu('feedback');         /** Remove the feedback link **/

    //$wp_admin_bar->remove_menu('site-name');      /** Remove the site name menu **/

    $wp_admin_bar->remove_menu('view-site');        /** Remove the view site link **/

    $wp_admin_bar->remove_menu('updates');          /** Remove the updates link **/

    $wp_admin_bar->remove_menu('comments');         /** Remove the comments link **/

    $wp_admin_bar->remove_menu('new-content');      /** Remove the content link **/

    $wp_admin_bar->remove_menu('w3tc');             /** If you use w3 total cache remove the performance link **/

    //$wp_admin_bar->remove_menu('my-account');     /** Remove the user details tab **/

}

add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

add_action( 'admin_menu', 'my_remove_menus', 999 );



function my_remove_menus() {



	remove_submenu_page( 'themes.php', 'themes.php' );


    remove_submenu_page( 'themes.php', 'customize.php' );

    remove_submenu_page( 'plugins.php', 'plugin-editor.php' );

    remove_submenu_page( 'index.php', 'update-core.php' );

    remove_submenu_page( 'options-general.php', 'options-discussion.php' );

}

/** Ẩn MenuBar **/

add_filter('show_admin_bar', '__return_false');

/** Thay doi duong dan logo admin **/

function wpc_url_login(){

return "http://Mã Số Xanh/";

}

add_filter('login_headerurl', 'wpc_url_login');

/** Thay doi logo admin wordpress **/

function login_css() {

wp_enqueue_style( 'login_css', plugin_dir_url( __FILE__ ) . '/login.css' );

}

add_action('login_head', 'login_css');



/** Thêm cột để chứa ảnh cho cả post và page **/

if (function_exists( 'add_theme_support' )){

    add_filter('manage_posts_columns', 'posts_columns', 5);

    add_action('manage_posts_custom_column', 'posts_custom_columns', 5, 2);

    add_filter('manage_pages_columns', 'posts_columns', 5);

    add_action('manage_pages_custom_column', 'posts_custom_columns', 5, 2);

}

function posts_columns($defaults){

    $defaults['wps_post_thumbs'] = __('Thumbs');

    return $defaults;

}

function posts_custom_columns($column_name, $id){

        if($column_name === 'wps_post_thumbs'){

        echo the_post_thumbnail( array(125,80) );

    }

}

/** Welcome **/

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

	  

	function my_custom_dashboard_widgets() {

	global $wp_meta_boxes;

	 

	wp_add_dashboard_widget('custom_help_widget', 'Giới thiệu', 'custom_dashboard_help');

	}

	 

	function custom_dashboard_help() { ?>

    <p>Chào mừng Quý khách đến với hệ thống Quản Trị Website.<p>

    <p><strong>THÔNG TIN WEBSITE</strong></p>

    <P><?php echo bloginfo( 'name' ); ?> | <?php echo bloginfo( 'description' ); ?></p>

    <p><strong>Địa chỉ: Đang cập nhật</p>

    <p><strong>Điện thoại: 01672149659 - 0944673526 </p>

    <p><strong>Email: devqanh@gmail.com - vnanhquan@gmail.com </strong><?php //echo bloginfo( 'admin_email' ); ?></p>

	<p><strong style="color: #ff0000;">NHÀ PHÁT TRIỂN</strong></p>

    <p>Hệ thống được phát triển bởi <strong style="color: #0083E0;"><a href="https://www.facebook.com/devqanh96" target="_blank">Quyền anh </a></strong> trên nền <strong> Wordpress <?php echo bloginfo("version") ; ?> </strong>.</p>

    <p>Mọi thắc mắc, lỗi trong quá trình sử dụng Quý khách hàng có thể liên hệ Kỹ Thuật <strong style="color: #0083E0;"><a href="https://www.facebook.com/devqanh96" target="_blank">Quyền anh </a></strong></p>

    <p><strong>Võ Quyền Anh</strong>

    <p> Web Developer</p> 

    <p><strong>Phone</strong>: 01672149659<strong> Skype</strong>: vnanhquan96</p>

    <p><strong>Email</strong>: devqanh.com <strong>Website</strong>: <a href="https://devqanh.com">devqanh.com</a></p>

    <p style="color: #0083E0;">  Cảm ơn quý khách đã tin tưởng và sử dụng sản phẩm của chúng tôi.</p>

	<?php }

/** This theme allows users to set a custom background. **/

add_theme_support( 'custom-background', apply_filters( 'kinmedia_custom_background_args', array(

		'default-color' => 'f5f5f5',

	) ) );

/** Add Setting **/    

$new_general_setting = new new_general_setting();

class new_general_setting {

    function new_general_setting( ) {

        add_filter( 'admin_init' , array( &$this , 'register_fields' ) );

    }

    function register_fields() {

        register_setting( 'general', 'info', 'esc_attr' );

        add_settings_field('info', '<label for="info">'.__('Name' , 'info' ).'</label>' , array(&$this, 'print_info_field') , 'general' );



        register_setting( 'general', 'address', 'esc_attr' );

        add_settings_field('address', '<label for="address">'.__('Address' , 'address' ).'</label>' , array(&$this, 'print_address_field') , 'general' );



        register_setting( 'general', 'tel', 'esc_attr' );

        add_settings_field('tel', '<label for="tel">'.__('Tel & Fax' , 'tel' ).'</label>' , array(&$this, 'print_tel_field') , 'general' );



        register_setting( 'general', 'email', 'esc_attr' );

        add_settings_field('email', '<label for="tel">'.__('Email' , 'email' ).'</label>' , array(&$this, 'print_email_field') , 'general' );     

    }

    function print_tel_field() {

        $value = get_option( 'tel', '' );

        echo '<input type="text" id="tel" style="width: 45%;" name="tel" value="' . $value . '" />';

    }

    function print_email_field() {

        $value = get_option( 'email', '' );

        echo '<input type="text" id="email" style="width: 45%;" name="email" value="' . $value . '" />';

    }

    function print_address_field() {

        $value = get_option( 'address', '' );

        echo '<input type="text" id="address" style="width: 45%;" name="address" value="' . $value . '" />';

    }

    function print_info_field() {

        $value = get_option( 'info', '' );

        echo '<input type="text" id="info" style="width: 45%;" name="info" value="' . $value . '" />';

    }

}
    // tat update core
    # 2.3 to 2.7:
add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_version_check' );" ), 2 );
add_filter( 'pre_option_update_core', create_function( '$a', "return null;" ) );

# 2.8+:
remove_action( 'wp_version_check', 'wp_version_check' );
remove_action( 'admin_init', '_maybe_update_core' );
add_filter( 'pre_transient_update_core', create_function( '$a', "return null;" ) );

# 3.0+:
add_filter( 'pre_site_transient_update_core', create_function( '$a', "return null;" ) );
// tat update plugin 
# 2.3 to 2.7:
add_action( 'admin_menu', create_function( '$a', "remove_action( 'load-plugins.php', 'wp_update_plugins' );") );
    # Why use the admin_menu hook? It's the only one available between the above hook being added and being applied
add_action( 'admin_init', create_function( '$a', "remove_action( 'admin_init', 'wp_update_plugins' );"), 2 );
add_action( 'init', create_function( '$a', "remove_action( 'init', 'wp_update_plugins' );"), 2 );
add_filter( 'pre_option_update_plugins', create_function( '$a', "return null;" ) );

# 2.8 to 3.0:
remove_action( 'load-plugins.php', 'wp_update_plugins' );
remove_action( 'load-update.php', 'wp_update_plugins' );
remove_action( 'admin_init', '_maybe_update_plugins' );
remove_action( 'wp_update_plugins', 'wp_update_plugins' );
add_filter( 'pre_transient_update_plugins', create_function( '$a', "return null;" ) );

# 3.0:
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );
 // tat update theme 
# 2.8 to 3.0:
remove_action( 'load-themes.php', 'wp_update_themes' );
remove_action( 'load-update.php', 'wp_update_themes' );
remove_action( 'admin_init', '_maybe_update_themes' );
remove_action( 'wp_update_themes', 'wp_update_themes' );
add_filter( 'pre_transient_update_themes', create_function( '$a', "return null;" ) );

# 3.0:
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );


