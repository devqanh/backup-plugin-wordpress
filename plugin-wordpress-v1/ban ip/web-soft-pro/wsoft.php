<?php

/*
  Plugin Name: Web soft pro
  Description: Plugin all
  Author: devqanh
  Version: 1.0
  Author URI: http://wsoftpro.com/


*/
require_once('option.php');

function create_count_text()
{
}

add_shortcode('count_text', 'create_count_text');

function simple_ip_ban()
{
    if (get_option('check_box_ip')) {
        $ipcheck = explode(",", get_option('get_text_ip'));
        $myip = $_SERVER['REMOTE_ADDR'];
        if (!in_array($myip, $ipcheck)) {
            get_template_part(404);
            exit;
        } elseif (!is_user_logged_in()) {
            wp_redirect('/wp-admin');
            die;
        }
    }
}

add_action('wp_head', 'simple_ip_ban');
function my_login_redirect($redirect_to, $request, $user)
{
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for admins
        if (in_array('administrator', $user->roles)) {
            // redirect them to the default place
            return $redirect_to;
        } else {
            return home_url();
        }
    } else {
        return $redirect_to;
    }
}

add_filter('login_redirect', 'my_login_redirect', 10, 3);



