<?php
function register_count_text_setting()
{
    register_setting('ban-ip-group', 'get_text_ip');
    register_setting('ban-ip-group','check_box_ip');

}

function count_text_create_menu()
{
    add_menu_page('Ban ip title', 'Ban ip menu ', 'administrator', __FILE__, 'ip_page_setting', 'dashicons-shield', 85);
    add_action('admin_init', 'register_count_text_setting');

}

add_action('admin_menu', 'count_text_create_menu');
function ip_page_setting()
{
    ?>
    <div class="wrap">
        <h2>Quản trị IP</h2>
        <?php if (isset($_GET['settings-updated'])) { ?>
            <div id="message" class="updated">
                <p><strong><?php _e('Save Changed') ?></strong></p>
            </div>
        <?php } ?>
        <form method="post" action="options.php">
            <?php settings_fields('ban-ip-group'); ?>

            <table class="form-table">
                <tr valign="top">
                    <th scope="row">List ip được phép truy cập</th>
                    <td><textarea  rows="10" cols="100" name="get_text_ip" placeholder=""
                        /><?php echo get_option('get_text_ip'); ?></textarea>
                    </td>
                </tr>
                <tr valign="top">
                <td scope="row">Bật tắt chức năng ban ip </td>
                    <td > <input type="checkbox" name="check_box_ip" <?php if(get_option('check_box_ip')) echo 'checked' ?>></td>
                </tr>

            </table>
            <?php submit_button(); ?>
        </form>
    </div>
<?php } ?>
