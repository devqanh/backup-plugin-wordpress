<?php
/*
Plugin Name: Tự động remove link 
Plugin URI: http://devqanh.com/
Description: Tự động remove link khi publish và giữ lại text .
Version: 1.0
Author: Quyền anh
Author URI: http://devqanh.com/
*/

function remove_links_from_post($post){
    $post_content = stripslashes($post["post_content"]);

    if(!preg_match_all("/(<a.*>)(.*)(<\/a>)/ismU",$post_content,$outbound_links,PREG_SET_ORDER)){
        return $post;
    }

    foreach($outbound_links as $key => $value){
        preg_match("/href\s*=\s*[\'|\"]\s*(.*)\s*[\'|\"]/i",$value[1],$href);

        if((substr($href[1],0,7)!="http://" && substr($href[1],0,8)!="https://") || substr($href[1],0,strlen(get_bloginfo("url")))==get_bloginfo("url")){
            unset($outbound_links[$key]);
        }else{
            $post_content = str_replace($outbound_links[$key][0],$outbound_links[$key][2],$post_content);
        }
    }

    $post["post_content"] = addslashes($post_content);

    return $post;
}

add_filter("wp_insert_post_data", "remove_links_from_post");
?>