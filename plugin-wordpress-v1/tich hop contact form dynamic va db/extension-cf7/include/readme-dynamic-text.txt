/*
Plugin Name: Contact Form 7 - Dynamic Text Extension
Plugin URI: http://sevenspark.com/wordpress-plugins/contact-form-7-dynamic-text-extension
Description: Provides a dynamic text field that accepts any shortcode to generate the content.  Requires Contact Form 7
Version: 2.0.2.1
Author: Chris Mavricos, SevenSpark
Author URI: http://sevenspark.com
License: GPL2
*/